`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.05.2019 17:25:39
// Design Name: 
// Module Name: huffman_decoder_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module huffman_decoder_tb;
    
  //  reg [7:0] symbols_data [0:26] = {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90};
  //  reg [7:0] symbols_amount = 27;
 //   reg [63:0] code_list_data [0:26] = {7, 65, 19, 129, 0, 36, 27, 26, 9, 1, 83, 4, 5, 51, 10, 14, 20, 12, 13, 2, 17, 11, 18, 33, 3, 30, 6};
 //   reg [63:0] input_data_stream_length = 169; 
 //   reg [7:0] codes_length_data [0:26] = {3, 7, 7, 8, 3, 6, 5, 5, 4, 8, 7, 6, 4, 6, 5, 5, 5, 4, 4, 5, 5, 5, 5, 6, 5, 5, 4}; 
         reg [7:0] symbols_amount = 50;
    wire [7:0] decoded_number;
    reg [63:0] input_data_stream_length = 298;     
    reg [63:0] code_list_data [0:49] = {7,122,83,26,27,58,42,30,20,4,10,100,5,75,50,1,60,24,13,18,29,51,28,11,61,33,14,0,90,43,38,9,68,74,36,25,8,6,62,2,12,21,40,49,35,34,19,3,17,22};  
    reg [7:0] codes_length_data [0:49] = {3,7,7,7,5,7,6,6,5,7,7,7,5,7,6,6,6,5,5,6,6,6,6,7,6,6,5,4,7,6,6,5,7,7,7,5,6,6,6,6,5,5,6,6,6,6,7,6,6,5};  

       reg [7:0] symbols_data [0:49] =  {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 114, 115, 116, 117, 119, 121, 122};
    
    reg clock;
    reg data_enable;
//    reg data_in [0:168] = {0,0,0,0,0,1,0,0,1,1,1,0,1,1,0,1,0,1,1,1,0,0,1,1,0,0,0,0,0,0,0,1,1,0,0,1,0,1,0,0,1,0,0,0,1,0,1,0,1,1,0,0,1,1,0,1,0,1,0,0,1,1,1,0,0,0,1,0,1,0,0,1,1,1,0,1,1,0,1,0,0,0,1,0,0,0,1,1,1,0,1,0,0,1,0,0,1,1,0,0,0,0,1,1,1,0,0,0,0,1,1,1,1,0,1,1,0,1,0,0,0,0,0,0,1,1,0,0,0,0,0,1,1,1,0,0,1,0,0,1,1,1,1,1,0,1,1,1,0,1,1,1,1,0,1,0,
//    0,1,0,0,1,0,0,0,0,0,1,0,1};
    
    reg data_in [0:297] = {1, 
    1, 
    1, 
    0, 
    1, 
    0, 
    1, 
    1, 
    1, 
    1, 
    1, 
    1, 
    0, 
    0, 
    1, 
    0, 
    1, 
    0, 
    1, 
    0, 
    1, 
    1, 
    0, 
    0, 
    1, 
    1, 
    0, 
    1, 
    1, 
    0, 
    1, 
    0, 
    1, 
    1, 
    1, 
    0, 
    0, 
    1, 
    0, 
    1, 
    0, 
    1, 
    0, 
    1, 
    1, 
    1, 
    1, 
    0, 
    0, 
    0, 
    1, 
    0, 
    1, 
    0, 
    0, 
    1, 
    0, 
    0, 
    0, 
    0, 
    0, 
    1, 
    0, 
    1, 
    0, 
    0, 
    0, 
    0, 
    0, 
    1, 
    0, 
    0, 
    1, 
    1, 
    1, 
    0, 
    1, 
    0, 
    0, 
    1, 
    1, 
    0, 
    1, 
    0, 
    0, 
    1, 
    0, 
    1, 
    0, 
    0, 
    1, 
    1, 
    1, 
    0, 
    0, 
    0, 
    0, 
    0, 
    0, 
    0, 
    1, 
    1, 
    1, 
    1, 
    0, 
    0, 
    0, 
    1, 
    1, 
    1, 
    0, 
    1, 
    1, 
    0, 
    0, 
    1, 
    0, 
    0, 
    1, 
    0, 
    1, 
    0, 
    1, 
    1, 
    1, 
    0, 
    1, 
    1, 
    0, 
    0, 
    1, 
    1, 
    0, 
    0, 
    1, 
    1, 
    1, 
    0, 
    1, 
    1, 
    0, 
    1, 
    0, 
    0, 
    0, 
    1, 
    0, 
    1, 
    1, 
    1, 
    1, 
    1, 
    0, 
    0, 
    0, 
    0, 
    1, 
    0, 
    1, 
    1, 
    1, 
    0, 
    0, 
    0, 
    0, 
    0, 
    0, 
    1, 
    0, 
    1, 
    1, 
    0, 
    1, 
    1, 
    1, 
    0, 
    1, 
    0, 
    1, 
    0, 
    1, 
    1, 
    0, 
    0, 
    1, 
    1, 
    0, 
    0, 
    1, 
    0, 
    0, 
    0, 
    1, 
    0, 
    0, 
    0, 
    1, 
    0, 
    1, 
    0, 
    1, 
    0, 
    0, 
    1, 
    0, 
    0, 
    1, 
    0, 
    0, 
    1, 
    0, 
    1, 
    0, 
    0, 
    1, 
    1, 
    0, 
    0, 
    0, 
    1, 
    0, 
    0, 
    0, 
    1, 
    1, 
    0, 
    0, 
    0, 
    0, 
    1, 
    1, 
    1, 
    1, 
    1, 
    0, 
    1, 
    0, 
    0, 
    0, 
    0, 
    0, 
    0, 
    1, 
    1, 
    0, 
    1, 
    0, 
    1, 
    0, 
    1, 
    0, 
    0, 
    0, 
    1, 
    0, 
    1, 
    1, 
    0, 
    0, 
    0, 
    1, 
    1, 
    1, 
    1, 
    0, 
    0, 
    0, 
    1, 
    0, 
    1, 
    0, 
    0, 
    0, 
    1, 
    1, 
    1, 
    0, 
    0, 
    1, 
    0, 
    0, 
    1, 
    1, 
    0, 
    0, 
    0, 
    0, 
    1, 
    0, 
    0, 
    0, 
    1, 
    0, 
    0, 
    1, 
    1, 
    0, 
    1};
    
    reg input_data_stream;
    reg [63:0] code_list_stream;
    reg [7:0] codes_length_stream;
    reg [7:0] symbols_stream;
    
    integer i = 64'h0;
    
    huffman_decoder decoder(
    .clock(clock),
    .data_enable(data_enable),
    .input_data_stream(input_data_stream),
    .input_data_stream_length(input_data_stream_length),
    .code_list_stream(code_list_stream),
    .codes_length_stream(codes_length_stream),
    .symbols_stream(symbols_stream),
    .symbols_amount(symbols_amount),
    .decoded_number(decoded_number)
    );
    
    // set clock in period 10ps -> #10
        initial begin
        forever #10 clock = ~clock;
        end
            
    // initialize inputs with enabling data in each 20ps -> #20
        initial begin
            clock = 0;
            data_enable = 0;
             
            for(i = 0; i < 50; i = i + 1) begin
                data_enable = 1;
                code_list_stream = code_list_data[i];
                codes_length_stream = codes_length_data[i];
                symbols_stream = symbols_data[i];
                #20;
                data_enable = 0;
            end
            data_enable = 0;
            #20
            
            for(i = 0; i < 298; i = i + 1) begin
                data_enable = 1;
                input_data_stream = data_in[i];
                #20;
                data_enable = 0;
            end
            
            #20; // delay 10ps
            input_data_stream = 'bz;
            code_list_stream = 'bz;
            codes_length_stream = 'bz;
            symbols_stream = 'bz;
            
            #1000000; //delay 2100ps to finish testing
            $finish; // inform that the test was finished
        end
    
endmodule
