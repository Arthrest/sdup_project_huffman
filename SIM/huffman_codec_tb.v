`timescale 1ns / 1ns

module huffman_codec_tb;

reg clock;
reg data_enable;
reg [7:0] data_in;
reg [7:0] data_count;
reg reset;
wire coded_out;
wire [7:0] decoded_out;
wire coded_data_ready;
wire decoded_data_ready;
reg ready_for_coded_data;
reg ready_for_decoded_data;

parameter symbols_amount = 50;

integer i = 32'h0;

reg [7:0] data_to_input [0:50-1];
initial begin: data_to_input_for_simulation
  $readmemh("symbols_data.dat", data_to_input);
end

huffman_codec #(symbols_amount) codec(
    .clock(clock),
    .reset(reset),
    .data_enable(data_enable),
    .data_in(data_in),
    .data_count(data_count),
    .coded_out(coded_out),
    .decoded_out(decoded_out),
    .coded_out_data_ready(coded_data_ready),
    .decoded_out_data_ready(decoded_data_ready),
    .ready_for_coded_data(ready_for_coded_data),
    .ready_for_decoded_data(ready_for_decoded_data)
    );

initial begin : clock_generator
  clock = 0;
  forever #5 clock = !clock;
end

initial begin : reset_signal
  reset = 1;
  #30 reset = 0;
end

    initial begin
        data_in = 0;
        data_enable = 0;
        data_count = 31;//50;
        ready_for_coded_data = 0;
        ready_for_decoded_data = 0;
        #40

         //data_enable = 1;

         for(i =0; i < data_count; i = i + 1) begin //for(i =0; i < 50; i = i + 1) begin
            data_enable = !data_enable;
            data_in = data_to_input[i];
            #10;
         end

         data_enable = !data_enable;

         while(coded_data_ready != 1 && coded_data_ready != 1) begin
         #10;
         end
         while(coded_data_ready || decoded_data_ready) begin
            if(coded_data_ready == 1)begin
                ready_for_coded_data = !ready_for_coded_data;
            end
            if(decoded_data_ready == 1)begin
                ready_for_decoded_data = !ready_for_decoded_data;
            end
            //$display("code: ", coded_out);
            #10;
         end

        //#10000000; //delay 2100ps to finish testing
        //$finish; // inform that the test was finished
    end

endmodule

