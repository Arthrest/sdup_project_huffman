`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.05.2019 20:57:09
// Design Name: 
// Module Name: huffman_coder_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module huffman_coder_tb;
    parameter data_stream_amount = 3;
    parameter symbols_amount_ = 50;

    reg [63:0] input_data_stream_length = 3; 
    reg [7:0] symbols_amount = 50;
    reg [7:0] symbols_data [0:symbols_amount_-1] =  {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 114, 115, 116, 117, 119, 121, 122};
    reg [63:0] code_list_data [0:symbols_amount_-1] = {7, 94, 35, 251, 8, 30, 27, 22, 17, 306, 123, 68, 5, 59, 10, 14, 36, 12, 13, 18, 29, 51, 28, 99, 3, 62, 26, 0, 114, 43, 54, 9, 50, 178, 4, 25, 83, 42, 46, 2, 20, 21, 60, 33, 11, 34, 19, 61, 1, 6};  
    reg [7:0] codes_length_data [0:symbols_amount_-1] = {3, 7, 7, 8, 4, 7, 6, 6, 5, 9, 8, 7, 5, 7, 6, 6, 6, 5, 5, 6, 6, 6, 6, 7, 6, 6, 5, 4, 7, 6, 6, 5, 9, 8, 7, 5, 7, 6, 6, 6, 5, 5, 6, 6, 6, 6, 7, 6, 6, 5 };  
    
    wire coded_bit_stream;
    
    // 10001001001110110  
    reg clock;
    reg data_enable;
    reg [7:0] data_in [0:data_stream_amount-1] = {69,  77,  67}; 
    reg [7:0] input_data_stream;
    reg [63:0] code_list_stream;
    reg [7:0] codes_length_stream;
    reg [7:0] symbols_stream;

    integer i = 64'h0;

    huffman_coder coder(
    .clock(clock),
    .data_enable(data_enable),
    .input_data_stream(input_data_stream),
    .input_data_stream_length(input_data_stream_length),
    .code_list_stream(code_list_stream),
    .codes_length_stream(codes_length_stream),
    .symbols_stream(symbols_stream),
    .symbols_amount(symbols_amount),
    .coded_bit_stream(coded_bit_stream)
    );

    // set clock in period 10ps -> #10
        initial begin
        forever #10 clock = ~clock;
        end

    // initialize inputs with enabling data in each 20ps -> #20
        initial begin
            clock = 0;
            data_enable = 1;
            // load init data for coding algorithm
            for(i = 0; i < symbols_amount; i = i + 1) begin
                data_enable = 1;
                code_list_stream = code_list_data[i];
                codes_length_stream = codes_length_data[i];
                symbols_stream = symbols_data[i];
                #20;
            end
                            // load input data to code
            for(i = 0; i < input_data_stream_length; i = i + 1) begin
                data_enable = 1;
                input_data_stream = data_in[i];
                #20;
            end
            
            #20; // delay 10ps
            input_data_stream = 'bz;
            code_list_stream = 'bz;
            codes_length_stream = 'bz;
            symbols_stream = 'bz;
            data_enable = 0;
            
            #100000000; //delay 2100ps to finish testing
            $finish; // inform that the test was finished
        end

endmodule
