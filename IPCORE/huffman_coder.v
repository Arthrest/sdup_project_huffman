
`timescale 1 ns / 1 ps

////////////////////////////////////////////////////////////////////////////////////////////
//                                CODER MODULE
////////////////////////////////////////////////////////////////////////////////////////////	  
module huffman_coder(
    clock,
    reset,
    coder_start,
    coder_data_enable,
    input_data_stream,
    input_data_stream_length,
    code_list_stream,
    codes_length_stream,
    symbols_stream,
    symbols_amount,
    coder_data_ready,
    coded_bit_stream
    );

// ports declaration
input clock, reset;
input coder_start;
input coder_data_enable;
input [7:0] input_data_stream;
input [7:0] input_data_stream_length;
input [8:0] code_list_stream;
input [3:0] codes_length_stream;
input [7:0] symbols_stream;
input [7:0] symbols_amount;
output reg coder_data_ready;
output reg coded_bit_stream;

parameter MAX_INPUT_DATA = 256;
parameter MAX_SYMBOLS = 50;

//State machine
parameter START = 4'h00, INIT = 4'h01, START2 = 4'h02, TRIGGER_2 = 4'h03,
          DATA_LOADING = 4'h04, DATA_LOADING_FINISH = 4'h05, CODING_1 = 4'h06,
          CODING_2 = 4'h07, CODING_3 = 4'h08, CODING_4 = 4'h09, SENDING_DATA = 4'h0A;
reg [3:0] state;

// register for previous state of data enable to toggle during data reading
reg prev_coder_data_enable;

// locals data lengths and symbols amount
reg [7:0] input_data_length;
reg [7:0] symbols_amount_var;

// arrays
reg [8:0] code_list [0:MAX_SYMBOLS-1];
reg [3:0] codes_lengths [0:MAX_SYMBOLS-1];
reg [7:0] symbols [0:MAX_SYMBOLS-1];
reg [7:0] input_data [0:MAX_INPUT_DATA-1];
reg [0:8*MAX_INPUT_DATA-1] code_bit_tab;

// iterators
reg [7:0] init_data_counter;
reg [7:0] character_stream_counter;
reg [15:0] coder_iterator;
reg [15:0] l;
reg [15:0] j;
reg [15:0] k;
reg [7:0] position;

////////////////////////////////////////////////////////////////////////////////////////////
//                                      ALWAYS
////////////////////////////////////////////////////////////////////////////////////////////
always @ (posedge clock)
begin
////////////////////////////////////////////////////////////////////////////////////////////
//                                ASYNCHRONOUS RESET
////////////////////////////////////////////////////////////////////////////////////////////
    if(reset==1'b1)
    begin
        coder_data_ready <= 1'b0;
        coded_bit_stream <= 1'b0;
        state <= START;
        input_data_length <= 8'h0;
        symbols_amount_var <= 8'h0;
        init_data_counter <= 8'h0;
        character_stream_counter <= 8'h0;
        coder_iterator <= 16'h0;
        prev_coder_data_enable <= 1'b0;
        j <= 16'h0;
        k <= 16'h0;
        l <= 16'h0;
        position <= 8'h0;
        code_bit_tab <= 2048'h0;
    end
    else
    begin
////////////////////////////////////////////////////////////////////////////////////////////
//                                STATE MACHINE
////////////////////////////////////////////////////////////////////////////////////////////
            case(state)
////////////////////////////////////////////////////////////////////////////////////////////
//                            START - WAITING FOR TRIGGER
////////////////////////////////////////////////////////////////////////////////////////////
                START: if(coder_start == 1'b1) state <= INIT; else state <= START;
                
////////////////////////////////////////////////////////////////////////////////////////////
//                             LOAD DATA TREE FORM CODEC
////////////////////////////////////////////////////////////////////////////////////////////
                INIT: begin
                    if(coder_data_enable == 1'b1) begin
                        symbols_amount_var = symbols_amount;
                        code_list[init_data_counter] = code_list_stream;
                        codes_lengths[init_data_counter] = codes_length_stream;
                        symbols[init_data_counter] = symbols_stream;
                        init_data_counter = init_data_counter + 1;             
                        if(init_data_counter < symbols_amount) state <= INIT;
                        else begin
                            state <= START2;
                            coder_iterator <= 0;
                            k <= 0;
                        end
                    end
                    else state <= INIT;
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                      WAITING FOR TRIGGER TO LOAD DATA STREAM
////////////////////////////////////////////////////////////////////////////////////////////
                START2: if(coder_start == 1'b1) state <= DATA_LOADING; else state <= START2;
                
////////////////////////////////////////////////////////////////////////////////////////////
//                                LOAD DATA STREAM FROM CODEC
////////////////////////////////////////////////////////////////////////////////////////////
                DATA_LOADING: begin
                    if(coder_data_enable == 1'b1) begin
                        input_data_length = input_data_stream_length;
                        input_data[character_stream_counter] = input_data_stream;
                        character_stream_counter = character_stream_counter + 1;
                        state = DATA_LOADING;
                    end
                    if(character_stream_counter == input_data_length) state <= CODING_1; else state <= DATA_LOADING;
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                              CODING DATA - STATE 1
////////////////////////////////////////////////////////////////////////////////////////////
                CODING_1: begin  
                    j = 0;
                    if(coder_iterator < input_data_length) begin
                        position <= 0;
                        state <= CODING_2;
                        end
                    else begin
                        state <= SENDING_DATA;
                        j = 0;
                    end
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                              CODING DATA - STATE 2
////////////////////////////////////////////////////////////////////////////////////////////
                CODING_2: begin               
                    if((symbols[position] != input_data[coder_iterator])) begin
                        position <= position + 1;
                        state <= CODING_2;
                    end
                    else begin
                        state <= CODING_3;
                        l <= 1;
                    end
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                              CODING DATA - STATE 3
////////////////////////////////////////////////////////////////////////////////////////////
                CODING_3: begin
                    if(j < codes_lengths[position]) begin
                        state <= CODING_4; 
                    end
                    else begin
                        coder_iterator <= coder_iterator + 1;
                        state <= CODING_1;
                    end
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                              CODING DATA - STATE 4
////////////////////////////////////////////////////////////////////////////////////////////
                CODING_4: begin
                    if((code_list[position] & l) != 0) begin
                        code_bit_tab[k] = 1;
                        k = k + 1;
                    end
                    else begin
                        code_bit_tab[k] = 0;
                        k = k + 1;
                    end
                    // multiply by 2
                    l = l << 1;
                    j = j + 1;
                    state = CODING_3;
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                        SEND CODDED DATA TO HUFFMAN CODEC
////////////////////////////////////////////////////////////////////////////////////////////
                SENDING_DATA: begin
                    if(j < k) begin
                        coder_data_ready <= 1'b1;
                        coded_bit_stream = code_bit_tab[j];
                        j = j + 1;
                        state <= SENDING_DATA;
                    end
                    else begin
                        coder_data_ready <= 1'b0;
                        state <= START;
                        character_stream_counter = 0;
                        coder_iterator = 0;
                        position=0;
                        j <= 0;
                        k <= 0;
                        l <= 0;
                        init_data_counter <= 8'b0;
                    end
                end
            endcase;
     end;
end;

endmodule


