
`timescale 1 ns / 1 ps
////////////////////////////////////////////////////////////////////////////////////////////
//                                DECODER MODULE
////////////////////////////////////////////////////////////////////////////////////////////	
module huffman_decoder(
    // signals
    clock,
    reset,
    decoder_start,
    decoder_data_enable,
    input_data_stream,
    input_data_stream_length,
    code_list_stream,
    codes_length_stream,
    symbols_stream,
    symbols_amount,
    decoder_data_ready,
    decoded_number
    );
    
input clock, reset;
input decoder_start;
input decoder_data_enable;
input input_data_stream;
input [16:0] input_data_stream_length;
input [8:0] code_list_stream;
input [3:0] codes_length_stream;
input [7:0] symbols_stream;
input [7:0] symbols_amount;
output reg decoder_data_ready;
output reg [7:0] decoded_number; 

parameter MAX_INPUT_DATA = 256;
parameter MAX_SYMBOLS = 50;
    
//State machine
parameter START = 4'h0a, START2 = 4'h0b, INIT = 4'h00, DATA_LOADING = 4'h01, DECODE_1 = 4'h02, DECODE_2 = 4'h03,
          DECODE_3 = 4'h04, DECODE_4 = 4'h05, DECODE_5 = 4'h06, DECODE_6 = 4'h07,
          DECODE_7 = 4'h08, DATA_SENDING = 4'h09;

reg [3:0] state;    
    
// input data steram
reg [7:0] input_data_length;  
reg input_data [0:8*MAX_INPUT_DATA-1];
// tree
reg [7:0] symbol_amount_var;  
reg [8:0] code_list [0:MAX_SYMBOLS-1];
reg [3:0] codes_lengths [0:MAX_SYMBOLS-1];
reg [7:0] symbols [0:MAX_SYMBOLS-1];
reg [7:0] decoded_characters_array [0:MAX_SYMBOLS-1];
  
// iterators  
reg [7:0] init_data_counter;  
reg [15:0] i;
reg [7:0] bit_position;
reg [7:0] code_position;
reg [7:0] code_index;
reg [7:0] bit_index;

// data steram
reg [7:0] character_length;
reg [7:0] decoded_characters_counter;
reg [15:0] bit_stream_loader;
reg [8:0] code_value;

reg [8:0] sequence_to_and;
        
////////////////////////////////////////////////////////////////////////////////////////////
//                                ALWAYS
////////////////////////////////////////////////////////////////////////////////////////////	
always @ (posedge clock) begin
////////////////////////////////////////////////////////////////////////////////////////////
//                              ASYNCHRONOUS RESET
////////////////////////////////////////////////////////////////////////////////////////////	
    if(reset==1'b1)
    begin
        state <= START;
        init_data_counter <= 8'b0;
        i <= 16'b0;
        character_length <= 8'b0;
        decoded_characters_counter <= 8'b0;
        bit_position <= 8'b0;
        sequence_to_and <= 9'h0;
        bit_stream_loader <= 16'h0;
    end
    else
    begin
////////////////////////////////////////////////////////////////////////////////////////////
//                                 STATE MACHINE
////////////////////////////////////////////////////////////////////////////////////////////
            case(state)
////////////////////////////////////////////////////////////////////////////////////////////
//                          WAITING FOR TRIGGER TO START
////////////////////////////////////////////////////////////////////////////////////////////
                START: if(decoder_start == 1'b1) state <= INIT; else state <= START;
                
////////////////////////////////////////////////////////////////////////////////////////////
//                           LOAD HUFFMAN TREE DATA FROM CODEC
////////////////////////////////////////////////////////////////////////////////////////////
                INIT: begin
                    if(decoder_data_enable == 1'b1) begin
                        symbol_amount_var = symbols_amount;
                        code_list[init_data_counter] = code_list_stream;
                        codes_lengths[init_data_counter] = codes_length_stream;
                        symbols[init_data_counter] = symbols_stream;
                        init_data_counter = init_data_counter + 1;
                        if(init_data_counter < symbols_amount) state <= INIT;
                        else begin
                            state <= START2;
                            character_length <= 1;
                            decoded_characters_counter <= 0;
                            bit_position <= 0;
                            i <= 0;
                        end
                    end
                end

////////////////////////////////////////////////////////////////////////////////////////////
//                  WAITING FOR TRIGGER TO START LOADING DATA STREAM
////////////////////////////////////////////////////////////////////////////////////////////
                START2: if(decoder_start == 1'b1) state <= DATA_LOADING; else state <= START2;
                
////////////////////////////////////////////////////////////////////////////////////////////
//                           LOAD DATA STREAM FROM CODEC MODULE
////////////////////////////////////////////////////////////////////////////////////////////
                DATA_LOADING: begin;
                    if(decoder_data_enable == 1'b1) begin
                        input_data_length = input_data_stream_length;
                        input_data[bit_stream_loader] = input_data_stream;
                        bit_stream_loader = bit_stream_loader + 1;
                        if(bit_stream_loader == input_data_length) begin
                            state <= DECODE_1;
                        end
                        else begin
                            state <= DATA_LOADING;
                        end
                    end
                    else begin
                        state <= DATA_LOADING;
                    end
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                               DECODING STATE 1
////////////////////////////////////////////////////////////////////////////////////////////
                DECODE_1: begin
                    if(bit_position < input_data_length) begin
                        state <= DECODE_2;
                        code_index <= character_length;
                        bit_index <= bit_position;
                        code_value <= 0;
                        code_position <= 0;
                    end
                    else begin
                       i <= 0;
                       state <= DATA_SENDING;
                    end    
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                               DECODING STATE 2
////////////////////////////////////////////////////////////////////////////////////////////
                DECODE_2: begin
                    if(code_index != 0) state <= DECODE_3; else state <= DECODE_4;
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                               DECODING STATE 3
////////////////////////////////////////////////////////////////////////////////////////////
                DECODE_3: begin
                    if(code_index-1 == 0) begin
                        code_value = code_value + input_data[bit_index];
                    end
                    else begin
                        code_value = code_value + ((2<<(code_index-2))*input_data[bit_index]);
                    end
                    code_index = code_index - 1;
                    bit_index = bit_index - 1;
                    state = DECODE_2; 
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                               DECODING STATE 4
////////////////////////////////////////////////////////////////////////////////////////////
                DECODE_4: begin
                    if(code_position < symbol_amount_var) begin
                        state <= DECODE_5;
                    end
                    else begin
                        state <= DECODE_1;
                        bit_position = bit_position + 1;
                    end
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                               DECODING STATE 5
////////////////////////////////////////////////////////////////////////////////////////////
                DECODE_5: begin
                    if(character_length == 0) begin
                        sequence_to_and = 0;
                    end
                    else begin
                        sequence_to_and = (2 << (character_length-1)) - 1;
                    end
                    state <= DECODE_6;
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                               DECODING STATE 6
////////////////////////////////////////////////////////////////////////////////////////////
                DECODE_6: begin
                    if(((code_list[code_position])&sequence_to_and) == code_value) begin
                        state <= DECODE_7;
                    end 
                    else begin
                        code_position <= code_position + 1;
                        state <= DECODE_4;
                    end   
                end
               
////////////////////////////////////////////////////////////////////////////////////////////
//                               DECODING STATE 7
////////////////////////////////////////////////////////////////////////////////////////////               
                DECODE_7: begin
                    if(codes_lengths[code_position] > character_length) begin
                        character_length = character_length + 1;
                    end
                    else begin
                        character_length = 1;
                        decoded_characters_array[decoded_characters_counter] = symbols[code_position];
                        decoded_characters_counter = decoded_characters_counter + 1;
                    end
                    code_position = symbol_amount_var + 1;
                    state = DECODE_4;
                end
                
////////////////////////////////////////////////////////////////////////////////////////////
//                       SEND DATA TO HUFFMAN CODEC MODULE
////////////////////////////////////////////////////////////////////////////////////////////                
                DATA_SENDING: begin
                    if(i < decoded_characters_counter) begin
                        decoder_data_ready = 1;
                        decoded_number = decoded_characters_array[i];
                        i = i + 1;
                        state = DATA_SENDING;
                    end
                    else begin
                        decoder_data_ready = 0;
                        init_data_counter = 0;
                        sequence_to_and = 9'b0;
                        state = START;
                        i=0;
                        character_length <= 8'b0;
                        decoded_characters_counter <= 8'b0;
                        bit_position <= 8'b0;
                        bit_stream_loader = 0;
                    end
                end
            endcase
     end
end

endmodule