
`timescale 1 ns / 1 ps

////////////////////////////////////////////////////////////////////////////////////////////
//                                CODEC MODULE
////////////////////////////////////////////////////////////////////////////////////////////	
module hffman_codec(     // signals
                    clock,
                    reset,
                    // CONTROL_INPUT
                    start,
                    tree_data_ready,
                    input_data_ready,
                    ready_for_coded_data,
                    ready_for_decoded_data,
                    // INPUT
                    input_data,
                    input_data_count,
                    // TREE
                    symbols,
                    codes,
                    codes_lengths,
                    symbols_count,
                    // OUTPUT CONTROL
                    coded_out_data_ready,
                    decoded_out_data_ready,
                    //OUTPUT
                    coded_out,
                    decoded_out
                    ); 
             
// MAIN INPUTS    
input clock, reset;
// CONTROL_INPUT
input start;
input tree_data_ready;
input input_data_ready;
input ready_for_coded_data;
input ready_for_decoded_data;
// INPUT OF DATA STREAM
input [7:0] input_data;
input [7:0] input_data_count;
// TREE DATA
input [7:0] symbols;
input [8:0] codes;
input [3:0] codes_lengths;
input [7:0] symbols_count;
// OUTPUT CONTROL
output reg coded_out_data_ready;
output reg decoded_out_data_ready;
//OUTPUT
output reg coded_out;
output reg [7:0] decoded_out;

//State machine
parameter INIT = 4'h00, LOAD_TREE_DATA_TRIGGER = 4'h01, LOAD_TREE_DATA = 4'h02, LOAD_DATA_TRIGGER = 4'h03, 
		  LOAD_DATA = 4'h04, CODER_START = 4'h05, CODER_LOAD_TREE_DATA = 4'h06, CODER_LOAD_INPUT_DATA = 4'h07, 
          CODER_READ_OUT = 4'h08, DECODER_START = 4'h09, DECODER_LOAD_TREE_DATA = 4'h0a, 
          DECODER_LOAD_INPUT_DATA = 4'h0b, DECODER_READ_OUT = 4'h0c, START_SEND_DATA = 4'h0d, 
          SEND_DATA = 4'h0e, END = 4'h0f;
reg [3:0] state;

// parameter of maximum element of tabs
parameter MAX_INPUT_DATA = 256;
parameter MAX_BIT_CODER_INPUT = 256*8;
parameter MAX_SYMBOLS_COUNT = 50;
       
// previous satates for toggling data ready
reg prev_input_data_ready;
reg prev_tree_data_ready;
reg prev_output_coded_ready;
reg prev_output_decoded_ready;
reg coder_data_enable_outside;    

// iterators
reg [7:0] i;
reg [7:0] j;

// registers using in data stream loading
reg [7:0] data_tab [0:MAX_INPUT_DATA-1];                // data where new signs are stored
reg [7:0] data_length;                                  // length of new data single stream

// registers using in huffman tree data
reg [7:0] symbols_tab [0:MAX_SYMBOLS_COUNT-1];          // tab where huffman tree symbols are stored
reg [8:0] codes_tab [0:MAX_SYMBOLS_COUNT-1];            // tab where codes from huffman tree are stored
reg [3:0] codes_lengths_tab [0:MAX_SYMBOLS_COUNT-1];    // tab where code's lengths from huffman tree are stored
reg [7:0] symbols_count_internal;                       // amount of symbols from huffman tree

// registers using in huffman coder
    // data control
reg coder_work_start;
wire coder_data_ready;
reg coder_data_ready_output;
reg [15:0] bit_counter;
    // data stream
reg [7:0] coder_data_input;
reg [8:0] coder_code_in;
reg [3:0] coder_code_length_in;
reg [7:0] coder_symbols_in;
reg [7:0] coder_symbols_amount;
reg [7:0] input_data_count_var;
wire coder_bit_output;


// registers using in huffman decoder
    // data control
reg decoder_work_start;
wire decoder_data_ready;
reg decoder_data_enable_outside;
    // data stream
reg [7:0] decoder_data_length_in;
reg decoder_bit_input [0:MAX_BIT_CODER_INPUT-1];
reg decoder_data_input;
reg [15:0] decoder_input_data_count;
reg [8:0] decoder_code_in;
reg [3:0] decoder_code_length_in;
reg [7:0] decoder_symbols_in;
reg [7:0] decoder_symbols_amount;
wire [7:0] decoder_output;
reg [7:0] decoded_characters[0:MAX_INPUT_DATA-1];
reg [7:0] decoded_characters_counter;

////////////////////////////////////////////////////////////////////////////////////////////
//                              CODER INSTANCE CONNECTION 
////////////////////////////////////////////////////////////////////////////////////////////
huffman_coder coder(
        // signals
        .clock(clock),
        .reset(reset),
        // input controls
        .coder_start(coder_work_start),
        .coder_data_enable(coder_data_enable_outside),
        // input data
        .input_data_stream(coder_data_input),
        .input_data_stream_length(input_data_count_var),
        // input huffman tree
        .code_list_stream(coder_code_in),
        .codes_length_stream(coder_code_length_in),
        .symbols_stream(coder_symbols_in),
        .symbols_amount(coder_symbols_amount),
        // output control
        .coder_data_ready(coder_data_ready),
        // output data
        .coded_bit_stream(coder_bit_output)
        );

////////////////////////////////////////////////////////////////////////////////////////////
//                          DECODER INSTANCE CONNECTION 
////////////////////////////////////////////////////////////////////////////////////////////

huffman_decoder decoder(
        // signals
        .clock(clock),
        .reset(reset),
        // input controls
        .decoder_start(decoder_work_start),
        .decoder_data_enable(decoder_data_enable_outside),    
        // input data
        .input_data_stream(decoder_data_input),
        .input_data_stream_length(decoder_input_data_count),
        // input huffman tree
        .code_list_stream(decoder_code_in),
        .codes_length_stream(decoder_code_length_in),
        .symbols_stream(decoder_symbols_in),
        .symbols_amount(decoder_symbols_amount),       
        .decoder_data_ready(decoder_data_ready),
        .decoded_number(decoder_output)   
);

////////////////////////////////////////////////////////////////////////////////////////////
//                                  CODEC ALWAYS
////////////////////////////////////////////////////////////////////////////////////////////
always @ (posedge clock or posedge reset)
begin
////////////////////////////////////////////////////////////////////////////////////////////
//                            CODEC ASYNCHRONOUS RESET
////////////////////////////////////////////////////////////////////////////////////////////
    if(reset==1'b1)
    begin
        coded_out_data_ready <= 1'b0;
        decoded_out_data_ready <= 1'b0;
        prev_input_data_ready <= 1'b0;
        prev_tree_data_ready <= 1'b0;
        prev_output_decoded_ready <= 1'b0;
        prev_output_coded_ready <= 1'b0;
        coder_data_enable_outside <= 1'b0;
        coder_work_start <= 1'b0;
        decoder_work_start <= 1'b0;
        decoded_characters_counter <= 0;
        i = 8'b0;
        j = 8'b0;
        state <= INIT;
    end
    else
    begin
////////////////////////////////////////////////////////////////////////////////////////////
//                              CODEC STATE MACHINE
////////////////////////////////////////////////////////////////////////////////////////////
            case(state)
////////////////////////////////////////////////////////////////////////////////////////////
//                                      INIT
////////////////////////////////////////////////////////////////////////////////////////////
                INIT: begin
                    if(start == 1'b1) state <= LOAD_TREE_DATA_TRIGGER; else state <= INIT;
                end
////////////////////////////////////////////////////////////////////////////////////////////
//                     LOAD DATA HUFFMAN TREE TRIGGER
////////////////////////////////////////////////////////////////////////////////////////////
				LOAD_TREE_DATA_TRIGGER: begin
					if((tree_data_ready ^ prev_tree_data_ready) == 1'b1) begin
						prev_tree_data_ready <= tree_data_ready;
						state <= LOAD_TREE_DATA;
					end
					else begin
						state <= LOAD_TREE_DATA_TRIGGER;
					end
				end
////////////////////////////////////////////////////////////////////////////////////////////
//                           LOAD HUFFMAN TREE TO CODEC
////////////////////////////////////////////////////////////////////////////////////////////
                LOAD_TREE_DATA: begin
                        if(i < symbols_count) begin
                            symbols_tab[i] = symbols;
                            codes_tab[i] = codes;
                            codes_lengths_tab[i] = codes_lengths;
                            symbols_count_internal = symbols_count;
                            i = i + 1;
                            state = LOAD_TREE_DATA_TRIGGER;
                         end
                        else begin
                        i = 8'b0;
                        state = LOAD_DATA_TRIGGER;
                        end
                end
////////////////////////////////////////////////////////////////////////////////////////////
//                           LOAD STREAM DATA TRIGGER
////////////////////////////////////////////////////////////////////////////////////////////
				LOAD_DATA_TRIGGER: begin
					if((input_data_ready ^ prev_input_data_ready) == 1'b1) begin
						prev_input_data_ready = input_data_ready;
                        state = LOAD_DATA;
					end
					else begin
						state <= LOAD_DATA_TRIGGER;
					end
				end
////////////////////////////////////////////////////////////////////////////////////////////
//                           LOAD STREAM OF DATA TO CODEC
////////////////////////////////////////////////////////////////////////////////////////////	
                LOAD_DATA: begin
                        if(i < input_data_count) begin
                            data_tab[i] = input_data;
                            data_length = input_data_count;
                            i = i + 1;
                            state = LOAD_DATA_TRIGGER;
                        end
                        else begin
                            i = 8'b0;
                            state = CODER_START;
                        end
                end
////////////////////////////////////////////////////////////////////////////////////////////
//                              CODER START TRIGGER
////////////////////////////////////////////////////////////////////////////////////////////	
                CODER_START: begin
                    coder_work_start = 1'b1;
                    state = CODER_LOAD_TREE_DATA; 
                end  
////////////////////////////////////////////////////////////////////////////////////////////
//                               CODER LOAD TREE DATA
////////////////////////////////////////////////////////////////////////////////////////////	           
                CODER_LOAD_TREE_DATA: begin
                     if(i < symbols_count_internal) begin
                          coder_work_start = 1'b0;
                          coder_data_enable_outside = 1'b1;                         
                          coder_code_in = codes_tab[i];
                          coder_code_length_in = codes_lengths_tab[i];
                          coder_symbols_in = symbols_tab[i];
                          coder_symbols_amount = symbols_count_internal;
                          i = i + 1;
                          state = CODER_LOAD_TREE_DATA;
                      end
                      else begin
                          i = 8'b0;
                          coder_data_enable_outside = 1'b0;
                          coder_work_start = 1'b1;
                          state = CODER_LOAD_INPUT_DATA;
                      end 
                end
////////////////////////////////////////////////////////////////////////////////////////////
//                         CODER LOAD INPUT STERAM OF DATA
////////////////////////////////////////////////////////////////////////////////////////////	  
                CODER_LOAD_INPUT_DATA: begin
                      if(i < data_length) begin
                          coder_data_enable_outside = 1'b1;   
                          coder_data_input = data_tab[i];
                          input_data_count_var = data_length;
                          i = i + 1;
                          state = CODER_LOAD_INPUT_DATA;
                      end
                      else begin
                          i <= 8'b0;
                          coder_data_enable_outside <= 1'b0;
                          coder_work_start <= 1'b0;
                          state <= CODER_READ_OUT;
                      end 
                end
////////////////////////////////////////////////////////////////////////////////////////////
//                             READ DATA FROM CODER
////////////////////////////////////////////////////////////////////////////////////////////	  
                CODER_READ_OUT: begin
                    coder_data_ready_output = coder_data_ready;
                    if(coder_data_ready_output == 1'b1) begin
                        decoder_bit_input[bit_counter] = coder_bit_output;
                        bit_counter = bit_counter + 1;
                        state <= CODER_READ_OUT;
                    end
                    else if(coder_data_ready_output == 0 && bit_counter > 0) begin
                        i = 8'b0;
                        decoder_data_length_in = bit_counter;
                        state = DECODER_START;
                    end
                end
////////////////////////////////////////////////////////////////////////////////////////////
//                              DECODER START TRIGGER
////////////////////////////////////////////////////////////////////////////////////////////	  
                DECODER_START: begin
                    decoder_work_start = 1'b1;
                    state = DECODER_LOAD_TREE_DATA; 
                end 
////////////////////////////////////////////////////////////////////////////////////////////
//                               DECODER LOAD TREE DATA
////////////////////////////////////////////////////////////////////////////////////////////	           
                DECODER_LOAD_TREE_DATA: begin
                     if(i < symbols_count_internal) begin
                          decoder_work_start = 1'b0;
                          decoder_data_enable_outside = 1'b1;                         
                          decoder_code_in = codes_tab[i];
                          decoder_code_length_in = codes_lengths_tab[i];
                          decoder_symbols_in = symbols_tab[i];
                          decoder_symbols_amount = symbols_count_internal;
                          i = i + 1;
                          state = DECODER_LOAD_TREE_DATA;
                      end
                      else begin
                          i <= 8'b0;
                          decoder_data_enable_outside <= 1'b0;
                          decoder_work_start <= 1'b1;
                          state <= DECODER_LOAD_INPUT_DATA;
                      end 
                end
////////////////////////////////////////////////////////////////////////////////////////////
//                          DECODER LOAD INPUT STREAM DATA
////////////////////////////////////////////////////////////////////////////////////////////	  
                DECODER_LOAD_INPUT_DATA: begin
                    if(i < bit_counter) begin
                          decoder_data_enable_outside = 1'b1;   
                          decoder_data_input = decoder_bit_input[i];
                          decoder_input_data_count = bit_counter;
                          i = i + 1;
                          state = DECODER_LOAD_INPUT_DATA; 
                    end
                    else begin
                          i <= 8'b0;
                          decoder_data_enable_outside <= 1'b0;
                          decoder_work_start <= 1'b0;
                          state <= DECODER_READ_OUT;
                    end
                end
////////////////////////////////////////////////////////////////////////////////////////////
//                            READ DATA FROM DECODER
////////////////////////////////////////////////////////////////////////////////////////////	                  
                DECODER_READ_OUT: begin
                    if(decoder_data_ready == 1'b1) begin
                        decoded_characters[decoded_characters_counter] = decoder_output;
                        decoded_characters_counter = decoded_characters_counter+1;
                        state = DECODER_READ_OUT;
                    end
                    else if(decoder_data_ready == 0 && decoded_characters_counter > 0) begin
                        state = START_SEND_DATA;
                    end
                end
////////////////////////////////////////////////////////////////////////////////////////////
//                        SEND DATA TO CORTEX PROCESSOR TRIGGER
////////////////////////////////////////////////////////////////////////////////////////////	                 
                START_SEND_DATA: begin
                    if(((prev_output_decoded_ready ^ ready_for_decoded_data) == 1'b1) || ((prev_output_coded_ready ^ ready_for_coded_data) == 1'b1)) begin
                        prev_output_decoded_ready = ready_for_decoded_data;
                        prev_output_coded_ready = ready_for_coded_data;                      
                        state = SEND_DATA;
                    end
                    else begin
                        state = START_SEND_DATA;
                    end
               end
////////////////////////////////////////////////////////////////////////////////////////////
//                           SEND DATA TO CORTEX PROCESSOR
////////////////////////////////////////////////////////////////////////////////////////////	  
               SEND_DATA: begin
                    if(i < bit_counter) begin
                        decoded_out_data_ready = 1'b1;
                        decoded_out = decoded_characters[i];
                        coded_out_data_ready = 1'b1;  
                        coded_out = decoder_bit_input[i];
                        i = i + 1;
                        state = START_SEND_DATA; 
                    end
                    else begin
                        coded_out_data_ready = 1'b0;
                        decoded_out_data_ready = 1'b0;
                        state = END;
                    end
               end
////////////////////////////////////////////////////////////////////////////////////////////
//                     FINISH STATE MACHINE AND RETURN TO FIRST STATE
////////////////////////////////////////////////////////////////////////////////////////////	            
               END: begin
                    if(start == 1'b0) begin
						state <= INIT; 
						i = 8'b0;
						j = 8'b0;
						prev_input_data_ready <= 1'b0;
						prev_tree_data_ready <= 1'b0;
						prev_output_coded_ready <= 1'b0;
						prev_output_decoded_ready <= 1'b0;
						coder_work_start <= 1'b0;
						decoder_work_start <= 1'b0;
						bit_counter <= 0;
						decoded_characters_counter <= 0;
					end
					else state <= END;
                end
            endcase;
     end;
end;
endmodule