`timescale 1ns / 1ns

`define DECODER_INIT            3'b000
`define DECODER_DATA_LOADING    3'b001
`define DECODING_STATE_1 3'b010
`define DECODING_STATE_2 3'b011
`define DECODING_STATE_3 3'b100
`define DECODER_DATA_SENDING    3'b101

module huffman_decoder(
    input wire clock,
    input wire reset,
    input wire decoder_data_enable,
    input wire input_data_stream,
    input wire [31:0] input_data_stream_length,
    input wire [31:0] code_list_stream,
    input wire [7:0] codes_length_stream,
    input wire [7:0] symbols_stream,
    input wire [7:0] symbols_amount,
    output reg decoder_data_ready,
    output reg [7:0] decoded_number
    );

    reg [2:0] state;
    //reg [1:0] decoding_state;

    reg [31:0] code_list [0:99];
    reg [7:0] codes_lengths [0:99];
    reg [7:0] symbols [0:99];
    reg input_data [0:999];
    reg [31:0] input_data_length;
    reg [7:0] symbol_amount_var;
    reg [7:0] decoded_characters_array [0:99];

    reg [31:0] bit_stream_loader;
    reg [31:0] init_data_counter;
    reg [31:0] decoded_characters_counter;
    reg [31:0] i;

    reg [31:0] bit_position;
    reg [31:0] code_position;

    reg [7:0] code_index;
    reg [7:0] bit_index;
    reg [63:0] code_value;
    reg [7:0] character_length;

    reg [7:0] sequence_to_and;

    always @(posedge clock or posedge reset) begin
        if(reset) begin
            state = `DECODER_INIT;
            bit_stream_loader = 32'h0;
            init_data_counter = 32'h0;
            decoded_characters_counter = 32'h0;
            i = 32'h0;
            bit_position = 32'h0;
            code_position = 32'h0;
            code_index = 8'h0;
            bit_index = 8'h0;
            code_value = 64'h0;
            character_length = 8'h0;
            sequence_to_and = 8'h0;
            //decoding_state = `DECODING_STATE_1;
        end
		else begin
		    case(state)
            `DECODER_INIT: begin
                if(decoder_data_enable) begin
                    input_data_length = input_data_stream_length;
                    symbol_amount_var = symbols_amount;
                    code_list[init_data_counter] = code_list_stream;
                    codes_lengths[init_data_counter] = codes_length_stream;
                    symbols[init_data_counter] = symbols_stream;
                    init_data_counter = init_data_counter + 1;
                    if(init_data_counter == symbol_amount_var) begin
                        character_length = 1;
                        decoded_characters_counter = 0;
                        i = 0;
                        bit_position = 0;
                        state = `DECODER_DATA_LOADING;
                     //   $display("Finished loading init data");
    //                    for(i=0;i<27;i=i+1) begin
    //                        $display("code_list: ", code_list[i]);
    //                        $display("codes_lengths: ", codes_lengths[i]);
    //                        $display("symbols: ", symbols[i]);
    //                    end
                    end
                    else begin
                        state = `DECODER_INIT;
                    end
                end
            end
            `DECODER_DATA_LOADING: begin
                if(decoder_data_enable) begin
                    input_data[bit_stream_loader] = input_data_stream;
                    bit_stream_loader = bit_stream_loader + 1;
                    if(bit_stream_loader == input_data_length) begin
                        state = `DECODING_STATE_1;
      //                  $display("Finished loading input data");
    //                    for(i=0;i<169;i=i+1) begin
    //                        $display("bit: ", input_data[i]);
    //                    end
                    end
                    else begin
                        state = `DECODER_DATA_LOADING;
                    end
                end
                else begin
                    state = `DECODER_DATA_LOADING;
                end
            end
                    `DECODING_STATE_1: begin
                        //$display("State 1");
                        if(bit_position < input_data_length) begin
                            state = `DECODING_STATE_2;
                            code_index = character_length;
                            bit_index = bit_position;
                            code_value = 0;
                            code_position = 0;
                        end
                        else begin
                            i = 0;
                            state = `DECODER_DATA_SENDING;
                        end
                    end
                    `DECODING_STATE_2: begin
                        //$display("State 2");
                        if(code_index != 0) begin
                            //code_value = code_value + ((2**(code_index-1))*input_data[bit_index]);
                            if(code_index-1 == 0) begin
                                code_value = code_value + input_data[bit_index];
                            end
                            else begin
                                code_value = code_value + ((2<<(code_index-2))*input_data[bit_index]);
                            end
                            code_index = code_index - 1;
                            bit_index = bit_index - 1;
                            state = `DECODING_STATE_2;
    //                        $display("code_value: ", code_value);
                        end
                        else begin
                            state = `DECODING_STATE_3;
                        end
                    end
                    `DECODING_STATE_3: begin
                        //$display("State 3");
                        //$display("Bit: ", input_data[bit_position]);
                        if(code_position < symbol_amount_var) begin
                            if(character_length == 0) begin
                                sequence_to_and = 0;
                            end
                            else begin
                                sequence_to_and = (2 << (character_length-1)) - 1;
                            end
                            if(((code_list[code_position])&sequence_to_and) == code_value) begin
                                if(codes_lengths[code_position] > character_length) begin
                                    //$display("I want more bits");
                                    character_length = character_length + 1;
                                end
                                else begin
                                    character_length = 1;
                                    decoded_characters_array[decoded_characters_counter] = symbols[code_position];
                                    decoded_characters_counter = decoded_characters_counter + 1;
                                    //$display("Decoded symbol: ", symbols[code_position]);
                                end
                                //break;
                                code_position = symbol_amount_var + 1;
                            end
                            else begin
                              code_position = code_position + 1;
                            end
                            state = `DECODING_STATE_3;
                        end
                        else begin
                            state = `DECODING_STATE_1;
                            bit_position = bit_position + 1;
                        end
                    end
            `DECODER_DATA_SENDING: begin
                if(i < decoded_characters_counter) begin
                    decoder_data_ready = 1;
                    decoded_number = decoded_characters_array[i];
                    i = i + 1;
                    state = `DECODER_DATA_SENDING;
                end
                else begin
                    decoder_data_ready = 0;
                    state = `DECODER_INIT;
                end
            end
            default: begin
            end
         endcase
		end


    end
endmodule