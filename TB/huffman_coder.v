`timescale 1ns / 1ns

`define CODER_INIT          3'b000
`define CODER_DATA_LOADING  3'b001
`define CODING_STATE_1      3'b010
`define CODING_STATE_2      3'b011
`define CODING_STATE_3      3'b100
`define CODER_SENDING_DATA  3'b101

module huffman_coder(
    input wire clock,
    input wire reset,
    input wire coder_data_enable,
    input wire [7:0] input_data_stream,
    input wire [7:0] input_data_stream_length,
    input wire [31:0] code_list_stream,
    input wire [7:0] codes_length_stream,
    input wire [7:0] symbols_stream,
    input wire [7:0] symbols_amount,
    output reg coder_data_ready,
    output reg coded_bit_stream
    );

    reg [31:0] input_data_length;
    reg [31:0] symbols_amount_xd;
    reg [2:0] state;
    //reg [1:0] coding_state;

    reg [31:0] code_list [0:99];
    reg [7:0] codes_lengths [0:99];
    reg [7:0] symbols [0:99];
    reg [7:0] input_data [0:999];
    reg [0:999] code_bit_tab;

    reg [31:0] init_data_counter;
    reg [31:0] character_stream_counter;
    reg [31:0] coder_iterator;
    reg [31:0] i;
    reg [31:0] l;
    reg [31:0] j;
    reg [31:0] k;

    reg [31:0] position;

    always @(posedge clock or posedge reset) begin
        if(reset) begin
            state = `CODER_INIT;
            input_data_length = 32'h0;
            symbols_amount_xd = 32'h0;
            init_data_counter = 32'h0;
            character_stream_counter = 32'h0;
            coder_iterator = 32'h0;
            i = 32'h0;
            l = 32'h0;
            j = 32'h0;
            k = 32'h0;
            position = 32'h0;
            code_bit_tab = 1000'h0;
            //coding_state = `CODING_STATE_1;
        end
		else begin
        case(state)
            `CODER_INIT: begin
                if(coder_data_enable)begin
                    input_data_length = input_data_stream_length;
                    symbols_amount_xd = symbols_amount;
                    code_list[init_data_counter] = code_list_stream;
                    codes_lengths[init_data_counter] = codes_length_stream;
                    symbols[init_data_counter] = symbols_stream;
                    init_data_counter = init_data_counter + 1;
                    if(init_data_counter == symbols_amount) begin
                        state = `CODER_DATA_LOADING;
                        coder_iterator = 0;
                        k = 0;
                       // $display("Finished loading init data");
                    end
                end
            end
            `CODER_DATA_LOADING: begin
                if(coder_data_enable)begin
                    input_data[character_stream_counter] = input_data_stream;
                    character_stream_counter = character_stream_counter + 1;
                    state = `CODER_DATA_LOADING;
                end
                if(character_stream_counter == input_data_length) begin
                    state = `CODING_STATE_1;
                    //$display("Finished loading input data");
//                    for(i=0;i<input_data_stream_length;i=i+1)$display("input characters: ", input_data[i]);
                end
            end
                    `CODING_STATE_1: begin
                        //$display("STATE 1");
                        j = 0;
                        if(coder_iterator < input_data_length) begin
                            position = 0;
                            state = `CODING_STATE_2;
                        end
                        else begin
                            state = `CODER_SENDING_DATA;
                            j = 0;
                        end
                    end
                    `CODING_STATE_2: begin
                        //$display("STATE 2");
                        if((symbols[position] != input_data[coder_iterator])) begin
                            position = position + 1;
                            state = `CODING_STATE_2;
                        end
                        else begin
                           state = `CODING_STATE_3;
                           l = 1;
                        end
                    end
                    `CODING_STATE_3: begin
                        //$display("STATE 3");
                        if(j < codes_lengths[position]) begin
                            if((code_list[position] & l) != 0) begin
                                //$display("1");
                                code_bit_tab[k] = 1;
                                k = k + 1;
                            end
                            else begin
                               // $display("0");
                                code_bit_tab[k] = 0;
                                k = k + 1;
                            end
                            // multiply by 2
                            l = l << 1;
                            j = j + 1;
                            state = `CODING_STATE_3;
                        end
                        else begin
                            coder_iterator = coder_iterator + 1;
                            state = `CODING_STATE_1;
                        end
                    end
            `CODER_SENDING_DATA: begin
                if(j < k) begin
                    //$display("SENDING");
                    coder_data_ready = 1;
                    coded_bit_stream = code_bit_tab[j];
                    j = j + 1;
                    state = `CODER_SENDING_DATA;
                end
                else begin
                    coder_data_ready = 0;
                    state = `CODER_INIT;
                end
            end
            default: begin
            end
        endcase
		end


    end
endmodule