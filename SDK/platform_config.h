/*
 * Huffman codec for SDwUP
 *  Created on: 29.05.2019
 *      Authors: Arkadiusz Balys, Kamil Kasperczyk
 *      Akademia Gorniczo-Hutnicza im. Stanislawa Staszica w Krakowie
 *      Project to subject: Systemy Dedykowane w Ukladach Programowalnych
 *
 *      file: platform_config.h
 */

#ifndef __PLATFORM_CONFIG_H_
#define __PLATFORM_CONFIG_H_

#define STDOUT_IS_PS7_UART
#define UART_DEVICE_ID 0
#endif
