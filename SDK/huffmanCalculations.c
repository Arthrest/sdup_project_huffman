/*
 * Huffman codec for SDwUP
 *  Created on: 29.05.2019
 *      Authors: Arkadiusz Balys, Kamil Kasperczyk
 *      Akademia Gorniczo-Hutnicza im. Stanislawa Staszica w Krakowie
 *      Project to subject: Systemy Dedykowane w Ukladach Programowalnych
 *
 *      file: huffmanCalculations.c
 */

/***************************** Include Files *********************************/
#include "huffmanCalculations.h"
#include <stdbool.h>

/**************************** user definitions ********************************/

int writeData(u8* data, u32 dataLength, u8* symbols, u16* codes, u8* codesLengths, u8 symbolsAmount){

u32 treeData = 0;
u32 inputData = 0;
u32 treeReady = TREE_DATA_READY_MASK;
u32 dataReady = INPUT_DATA_READY_MASK;

//	Start huffman algorithm using start_data pulse
HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, INPUT_CONTROL_REG, START_DATA_MASK);
HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, INPUT_CONTROL_REG, 0);

for(int i=0; i < symbolsAmount; i++){
	// Prepare huffman tree
	treeData = 0;
	treeData |= (u32)symbols[i] << SYMBOLS_SHIFT;
	treeData |= (u32)codes[i] << CODES_SHIFT;
	treeData |= (u32)codesLengths[i] << CODES_LENGTH_SHIFT;
	treeData |= (u32)symbolsAmount << SYMBOLS_COUNT_SHIFT;

	// Send tree to huffman core
	HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, TREE_REG, treeData);
	//	Start sending tree data using tree_input_data pulse
	HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, INPUT_CONTROL_REG, treeReady);
	treeReady = (~treeReady) & TREE_DATA_READY_MASK;
}

// send one more pulse to trigger next state
HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, INPUT_CONTROL_REG, treeReady);
HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, TREE_REG, 0);

for(int i=0; i < dataLength; i++){
	// prepare data
	inputData = 0;
	inputData |= (u32)(data[i] << INPUT_DATA_SHIFT);
	inputData |= (u32)(dataLength << INPUT_DATA_COUNT_SHIFT);

	// Send data to huffman core
	HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, INPUT_REG, inputData);
	//	Start sending data using input_data pulse
	HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, INPUT_CONTROL_REG, dataReady);
	dataReady = (~dataReady) & INPUT_DATA_READY_MASK;
}

HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, INPUT_CONTROL_REG, dataReady);
// clear at the end
HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, INPUT_CONTROL_REG, 0);
HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, INPUT_REG, 0);

	return 0;
}

int readData(u8* coded, u8* decoded, u8 length){
	u32 result;
	u32 ready = (READY_FOR_CODED_DATA_MASK | READY_FOR_DECODED_DATA_MASK);
	u32 i = 0;
	bool timeout = false;
	int codedCounter = 0;

	while(1){
		HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, INPUT_CONTROL_REG, ready);
		//Wait for ready bit in status register
		while(((HUFFMAN_CODEC_IP_mReadReg(HUFFMAN_CODEC_IP_BASE_ADDR, OUTPUT_CONTROL_REG) & DECODED_OUT_DATA_READY_MASK) == 0)
				|| ((HUFFMAN_CODEC_IP_mReadReg(HUFFMAN_CODEC_IP_BASE_ADDR, OUTPUT_CONTROL_REG) & CODED_OUT_DATA_READY_MASK) == 0)){
			if(i>1000000){
				timeout = true;
				break;
			}
			i++;
		}
		if(timeout){
			break;
		}

		//Get results
		result = HUFFMAN_CODEC_IP_mReadReg(HUFFMAN_CODEC_IP_BASE_ADDR, OUTPUT_REG);
		//Extract sin and cos from 32-bit register data
		coded[codedCounter] = (u8)((result & CODED_OUT_MASK) >> CODED_OUT_SHIFT);
		if(codedCounter<length){
			decoded[codedCounter] = (u8)((result & DECODED_OUT_MASK) >> DECODED_OUT_SHIFT);
		}
		codedCounter++;
		ready = (~ready) & (READY_FOR_CODED_DATA_MASK | READY_FOR_DECODED_DATA_MASK);
	}

	HUFFMAN_CODEC_IP_mWriteReg(HUFFMAN_CODEC_IP_BASE_ADDR, INPUT_CONTROL_REG, ready);



		return codedCounter;
}
