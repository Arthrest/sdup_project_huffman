/*
 * Huffman codec for SDwUP
 *  Created on: 29.05.2019
 *      Authors: Arkadiusz Balys, Kamil Kasperczyk
 *      Akademia Gorniczo-Hutnicza im. Stanislawa Staszica w Krakowie
 *      Project to subject: Systemy Dedykowane w Ukladach Programowalnych
 *
 *      file: huffman_tree.c
 */

#define SYMBOLS_COUNT (u8)50
#define TREE_LOCAL_LENGTH (u8)((2*SYMBOLS_COUNT) - 1)

#include "huffman_tree.h"

/*
 * bubble_sort - sorting table using bubble sort algorithm
 * @param length - length of the table
 * @param symbols - pointer to begin of table to sort
 * @param probabilities - pointer to tab of symbol's probabilities
 * @param link	- pointer to table of linked symbols
 */
static void bubbleSort (u32 length, u32 *symbols, u32 *probabilities, u32* link){
	u32 i = 0;
	u32 temp1, temp2, temp3;
	while (i < length){
		for (u32 j = 0; j < length - 1; j++){
			if (probabilities[j] >= probabilities[j + 1] && probabilities[j] > 0){
				temp1 = probabilities[j];
                temp2 = symbols[j];
                temp3 = link[j];
                probabilities[j] = probabilities[j + 1];
                symbols[j] = symbols[j + 1];
                link[j] = link[j + 1];
                probabilities[j + 1] = temp1;
                symbols[j + 1] = temp2;
                link[j + 1] = temp3;
            }
        }
      i++;
    }
}

/*
 * search - search symbol in the tab
 * @param tab - pointer to table in which algorithm will search the symbol
 * @param length - length of input table
 * @param value - searching value
 * @return position of searching value in the table
 */
static u32 search(u32* tab, u32 length, u32 value){
	u32 it = 0;
    for(u32 i = 0; i < length; i++){
        if(value != tab[i]){
            it++;
        }
        else{
            break;
        }
    }
    return it;
}

void build_tree(u32* symbols, u32* probabilities, u8 symbolsCount, u16* codesList, u8* codesLengths){

	u32 length = TREE_LOCAL_LENGTH;
	u32 ID[TREE_LOCAL_LENGTH];
	u32 P[TREE_LOCAL_LENGTH];
	u32 Link[TREE_LOCAL_LENGTH];

	/// inicjalizacja tablic
	/// potrzebujemy:
	/// na wej�ciu:
	/// 1. tablice symboli: symbol_tab[ilosc_symboli]
	/// 2. tablice prawdopodobienstw: prob_tab[ilosc_symboli]
	/// 3. tablice symboli wraz z dodatkowym miejsce na przetwarzanie w przechodzeniu drzewa : Id[ilosc_symboli*2-1]
	/// 4. tablice prawdopodobienstw wraz z dodatkowym miejscem na przetwarzanie w przechodzeniu drzewa : P[ilosc_symboli*2-1]
	/// 5. tablice par potrzebna do przetwarzania w przechodzeniu drzewa : Link[ilosc_symboli*2-1]
	/// 6. tablice na kody : code_list[ilosc_symboli]
	/// 7. tablice na dlugosc kodow: code_length[ilosc_symboli]

	/// Wszystkie tablice inicjalizujemy zerami albo stanami nieustalonymi aby pozniej w obliczeniach nie bylo problemow
    for(u32 i = 0; i < length; i++){
    	ID[i] = 0;
        P[i] = 0;
        Link[i] = 0;
        if(i < SYMBOLS_COUNT){
            codesList[i] = 0;
            codesLengths[i] = 0;
        }
    }

    /// ladujemy na miejsca 1 - N symbole i prawdopodobienstwa w do tablic Id oraz P
    for(u32 i = 0; i < symbolsCount; i++){
    	ID[i] = symbols[i];
        P[i] = probabilities[i];
    }

    /// inicjalizujemy iteratory potrzebne do budowania drzewa
    u32 it = 0;
    u32 counter = 0;

    /// tworzenie drzewa
    /// 1. posortuj tablice
    /// 2. znajdz dwa symbole z najmniejszymi prawdopodobinstwami - pierwszy i drugi element z tablicy
    /// 3. w tablicy Id na kolejnym pustym miejscu [symbolsCount+id] wstaw nowy indeks - moze byc dowolna liczba, nieostotne jaka
    /// 4. w tablic P na tym samym miejsciu [symbolsCount+id] wstaw sume prawdopodbienstw dwoch znalezionych najmniejszych prawdopodbienstw
    /// 5. w tablicy P na miejscu pierwszego z znalezionych najmniejszych prawdopodobienstw [counter] wstaw 0 a na drugim miejscu [counter+1] wstaw 1 (w przypadku tego programu jest to -1 - sprawa do roziwazania)
    /// 6. w tablicy Link na miejscach znalezionych najmniejszych prawdopodbienstw Link[counter] & Link[counter+1] wartosc nowo utworzonego Id[symbolsCount+id]
    /// powtarzaj kroki 1- 6 zwiekszajac iterator tablicy o 1 oraz iterator counter o 2 az przeleci liczba dlugosci symboli - 1
    /// osiagniete tablice powinny miec przykladowy format:
    /// Id:   | A | B | C | D | E | 5 | 6 | 7 | 8  |
    /// P:    | 0 | 1 | 0 | 1 | 0 | 1 | 0 | 1 | 10 |
    /// Link  | 5 | 5 | 6 | 6 | 7 | 7 | 8 | 8 | 0  |

    for(u32 i=0; i < symbolsCount - 1; i++){
    	bubbleSort(symbolsCount+it, ID, P, Link);
    	ID[symbolsCount+it] = symbolsCount*10 + it + 1;     /// (3)
        P[symbolsCount+it] = P[counter] + P[counter+1];   	/// (4)
        P[counter] = 0;                             		/// (5)
        P[counter+1] = 1;                          			/// (5)
        Link[counter] = ID[symbolsCount+it];              	/// (6)
        Link[counter+1] = ID[symbolsCount+it];            	/// (6)
        it++;
        counter = counter + 2;
    }

    /// przechodzenie drzewa
    /// algorytm
    /// potrzebujemy dwa iteratory
    /// jeden do iteracji kodow - za jeden przebieg petli da sie zakodowac jeden symbol
    /// drugi iterator do przechodzenia tablicy It
    /// 1. zapisz do zmiennej tymczasowej symbol wartosc kolejnego symbolu z tablicy symbolow
    /// 2. zapisz do zmiennej current_symbol wartosc obecnego symbolu zmienna ta bedzie wykorzystywana do kolejnych iteracji
    /// 3. oblicz pozycje wybranego symbolu w tablicy Id - algorytm w funkcji na gorze
    /// 4. iezeli P[obliczonej pozycji] jest rowny 0 przesun bitowo tablice kodow o 1 (mnozymy razy 2)i dodaj 0
    /// 5. jezeli P[obliczonej pozycji] jest rowny 1(tutaj chwilowo -1) przesun bitowo tablice kodow o 1(mnozymy razy 2) i dodaj 1
    /// 6. zwieksz dlugosc danego symbolu o 1
    /// 7. zamien obecny symbol na ten z tablicy Link z obliczonej pozycji
    /// 8. jesli uzyskany nowy symbol nie jest rowny ostatniemu z drzewa z tablicy Id[ilosc_symboli*2-1] powtorz kroki 3 - 7
    /// 9. zwieksz iterator symbolowy dopoki nie przeleci wszystkich symboli - ilosc_symboli

    u32 symbolIt = 0;
    u32 symbol = 0;
    u32 position = 0;
    u32 currentSymbol = 0;
    u32 lastSymbol = ID[length-1];

    for(u32 i=0; i < symbolsCount; i++){
        symbol = symbols[symbolIt];                             		/// (1)
			currentSymbol = symbol;                                     /// (2)
			codesList[symbolIt] = 0;
			while(currentSymbol != lastSymbol){                       	/// (8)
				position = search(ID, length, currentSymbol);       	/// (3)
				if(P[position] == 0){                                   /// (4)
					codesList[symbolIt]  *= 2;                          /// (4)
				}else if(P[position] == 1){                             /// (5)
					codesList[symbolIt] *= 2;                           /// (5)
					codesList[symbolIt] += 1;                           /// (5)
				}
				codesLengths[symbolIt] = codesLengths[symbolIt] + 1;    /// (6)
				currentSymbol = Link[position];                         /// (7)

        }
        symbolIt++;                                                		/// (9)
     }

}
