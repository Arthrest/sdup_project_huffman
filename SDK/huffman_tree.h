/*
 * Huffman codec for SDwUP
 *  Created on: 29.05.2019
 *      Authors: Arkadiusz Balys, Kamil Kasperczyk
 *      Akademia Gorniczo-Hutnicza im. Stanislawa Staszica w Krakowie
 *      Project to subject: Systemy Dedykowane w Ukladach Programowalnych
 *
 *      file: huffman_tree.h
 */

#ifndef HUFFMAN_TREE_H_
#define HUFFMAN_TREE_H_

#include "platform.h"
#include "xil_printf.h"

/*
 * build_tree - build a tree using huffman algorithm
 * @param symbols - pointer to symbols table
 * @param probabilities - pointer to probabilities table
 * @param symbolsCount - symbols amount
 * @param codeList - pointer to output codeList table
 * @param codesLengths - pointer to output codes length table
 */
void build_tree(u32* symbols, u32* probabilities, u8 symbolsCount, u16* codesList, u8* codesLengths);


#endif /* HUFFMAN_TREE_H_ */
