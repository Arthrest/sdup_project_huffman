/*
 * Huffman codec for SDwUP
 *  Created on: 29.05.2019
 *      Authors: Arkadiusz Balys, Kamil Kasperczyk
 *      Akademia Gorniczo-Hutnicza im. Stanislawa Staszica w Krakowie
 *      Project to subject: Systemy Dedykowane w Ukladach Programowalnych
 *
 *      file: main.c
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "huffmanCalculations.h"
#include "huffman_tree.h"

#define READ_STRING_LENGTH 256
#define CODDED_LIMIT 10000
#define SYMBOLS_AMOUNT 50
#define ONE_PACKET_SIZE 32


static u32 readStreamOfNumbers(u8* data){
    if(data != NULL){
        u32 i = 0;
        do{
            data[i] = inbyte();
            i++;
        } while(((char8)data[i-1]) != '\n');
        return i-2;
    }
    return 0;
}

static u32 searchPosition(u8* tab){
	u32 position = 0;
	u32 i = 0;
	while(tab[i] != 2){
		i++;
		position ++;
	}
	return position;
}

int main(){
init_platform();

u8 coded_number[CODDED_LIMIT];
u32 codedCounter = 0;

u8 symbolsAmount = SYMBOLS_AMOUNT;
u8 symbols_to_fpga[SYMBOLS_AMOUNT] = {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 114, 115, 116, 117, 119, 121, 122};

// tree data
u16 codes[SYMBOLS_AMOUNT];
u8 codesLength[SYMBOLS_AMOUNT];
u32 symbols[SYMBOLS_AMOUNT] = {32, 44, 46, 59, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 82, 83, 84, 85, 87, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 114, 115, 116, 117, 119, 121, 122};
u32 probabilities[SYMBOLS_AMOUNT] = {1720, 90, 90, 50, 400, 65, 190, 150, 345, 5, 50, 50, 350, 95, 135, 155, 120, 235, 355, 120, 175, 190, 120, 90, 180, 160, 290, 400, 65, 190, 150, 345, 5, 50, 50, 350, 95, 135, 155, 120, 235, 355, 120, 175, 190, 120, 90, 180, 160, 290};

//////////////////////////////// start build huffman tree ///////////////////////////////////////
build_tree(symbols, probabilities, SYMBOLS_AMOUNT, codes, codesLength);

xil_printf("huffman tree:\n");
xil_printf("symbols: \n");
for(u32 i=0; i < SYMBOLS_AMOUNT; i++){
xil_printf(" %c - %d - %d\n", symbols[i], symbols[i], codes[i]);
}
xil_printf("\n");
//////////////////////////////// end build huffman tree///////////////////////////////////////

u8 streamOfNumbers[READ_STRING_LENGTH];

for(u32 i =0; i < CODDED_LIMIT; i++){
	coded_number[i] = 2;
}

	while(1){
		print("Enter text to be coded and confirm it with \"Enter\" button\n");
		u8 streamLength = readStreamOfNumbers(streamOfNumbers);
		print("Got input string\n");

		u8 decoded_number[streamLength];
		for(int i=0; i< streamLength; i++){
			decoded_number[i] = 0;
		}
		codedCounter = 0;

		xil_printf("Entered string:\n");
		for(uint32_t i=0; i<streamLength; i++){
			xil_printf("%c", streamOfNumbers[i]);
		}

		xil_printf("\nstream length: %d", streamLength);
		xil_printf("\n");
		//////////////////////////////// start communication between Cortex and FPGA ///////////////////////////////////////

		u32 offset = 0;
		u32 position = 0;
		u16 originStreamLength = streamLength;

		do{
			position = searchPosition(coded_number);
			if(streamLength > ONE_PACKET_SIZE){
				writeData(streamOfNumbers + offset, ONE_PACKET_SIZE, symbols_to_fpga, codes, codesLength, symbolsAmount);
				codedCounter += readData(coded_number + position, decoded_number + offset, streamLength);
				offset += ONE_PACKET_SIZE;
				streamLength -= ONE_PACKET_SIZE;
			}else{
				writeData(streamOfNumbers + offset, streamLength, symbols_to_fpga, codes, codesLength, symbolsAmount);
				codedCounter += readData(coded_number + position, decoded_number + offset, streamLength);
				streamLength -= streamLength;
			}

		} while(streamLength != 0);


		//////////////////////////////// end communication between Cortex and FPGA ///////////////////////////////////////

		xil_printf("Coded stream: ");
		for(uint32_t i=0; i<codedCounter; i++){
			xil_printf("%d", coded_number[i]);
		}
		xil_printf("\n");

		xil_printf("Decoded stream: ");
		for(uint32_t i=0; i<originStreamLength; i++){
			xil_printf("%c", decoded_number[i]);
		}
		xil_printf("\n");

		xil_printf("Bites length without coding: %d\n", originStreamLength*8);
		xil_printf("Bites length with coding: %d\n", codedCounter);
		xil_printf("Coding factor: %d%%\n", ((100*codedCounter)/(originStreamLength*8)));
	}
}

