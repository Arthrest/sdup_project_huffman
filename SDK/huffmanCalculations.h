/*
 * Huffman codec for SDwUP
 *  Created on: 29.05.2019
 *      Authors: Arkadiusz Balys, Kamil Kasperczyk
 *      Akademia Gorniczo-Hutnicza im. Stanislawa Staszica w Krakowie
 *      Project to subject: Systemy Dedykowane w Ukladach Programowalnych
 *
 *      file: huffmanCalculations.h
 */
#ifndef HUFFMANCALCULATIONS_H_
#define HUFFMANCALCULATIONS_H_

#include "xil_io.h"
#include "xparameters.h"
#include "huffman_codec_ip.h"


// base address for huffman codec
#define HUFFMAN_CODEC_IP_BASE_ADDR      XPAR_HUFFMAN_CODEC_IP_0_S00_AXI_BASEADDR
// registers for huffman codec
#define INPUT_CONTROL_REG     	 		HUFFMAN_CODEC_IP_S00_AXI_SLV_REG0_OFFSET
#define INPUT_REG		    	 		HUFFMAN_CODEC_IP_S00_AXI_SLV_REG1_OFFSET
#define TREE_REG   	 			 		HUFFMAN_CODEC_IP_S00_AXI_SLV_REG2_OFFSET
#define OUTPUT_CONTROL_REG		       	HUFFMAN_CODEC_IP_S00_AXI_SLV_REG3_OFFSET
#define OUTPUT_REG		     		  	HUFFMAN_CODEC_IP_S00_AXI_SLV_REG4_OFFSET

//////////////////////////////// masks ///////////////////////////////////////
// data masks CONTROL
#define START_DATA_MASK 				(u32)(0x01)
#define TREE_DATA_READY_MASK	 		(u32)(0x02)
#define INPUT_DATA_READY_MASK			(u32)(0x04)
#define READY_FOR_CODED_DATA_MASK		(u32)(0x08)
#define READY_FOR_DECODED_DATA_MASK		(u32)(0x10)
// data masks INPUT
#define INPUT_DATA_MASK					(u32)(0x00FF)
#define INPUT_DATA_COUNT_MASK			(u32)(0xFF00)
// data masks TREE
#define SYMBOLS_MASK					(u32)(0x000000FF)
#define CODES_MASK						(u32)(0x0001FF00)
#define CODES_LENGTH_MASK				(u32)(0x001E0000)
#define SYMBOLS_COUNT_MASK				(u32)(0x1FE00000)
// data masks OUTPUT CONTROL
#define CODED_OUT_DATA_READY_MASK		(u32)(0x01)
#define DECODED_OUT_DATA_READY_MASK		(u32)(0x02)
// data masks OUTPUT
#define CODED_OUT_MASK					(u32)(0x001)
#define DECODED_OUT_MASK				(u32)(0x1FE)
///////////////////////////////// shifts ///////////////////////////////////////
// data masks CONTROL
#define START_DATA_SHIFT 				(0)
#define TREE_DATA_READY_SHIFT	 		(1)
#define INPUT_DATA_READY_SHIFT			(2)
#define READY_FOR_CODED_DATA_SHIFT		(3)
#define READY_FOR_DECODED_DATA_SHIFT	(4)
// data SHIFTs INPUT
#define INPUT_DATA_SHIFT				(0)
#define INPUT_DATA_COUNT_SHIFT			(8)
// data SHIFTs TREE
#define SYMBOLS_SHIFT					(0)
#define CODES_SHIFT						(8)
#define CODES_LENGTH_SHIFT				(17)
#define SYMBOLS_COUNT_SHIFT				(21)
// data SHIFTs OUTPUT CONTROL
#define CODED_OUT_DATA_READY_SHIFT		(0)
#define DECODED_OUT_DATA_READY_SHIFT    (1)
// data SHIFTs OUTPUT
#define CODED_OUT_SHIFT					(0)
#define DECODED_OUT_SHIFT				(1)


/*
 * writeData - write data from Cortex processor to FPGA codec module
 * @param data - pointer to data stream
 * @param dataLength - size of stream
 * @param symbols - pointer to table of symbols
 * @param codes - pointer to table of codes generated using huffman tree algorithm
 * @param codesLength - pointer to table of code's lengths generated using huffman tree algorithm
 * @param symbolsAmount - number of symbols in current Language
 * @return 0 if everything finished successfully, 1 otherwise
 */
int writeData(u8* data, u32 dataLength, u8* symbols, u16* codes, u8* codesLengths, u8 symbolsAmount);

/*
 * readData - read data from FPGA and store in Cortex processor
 * @param codded - pointer to the table, where coded will be stored
 * @param decoded - pointer to the table where decoded symbols will be stored
 * @param length - length of expected data - for debug only
 * @return amount of coded bits
 */
int readData(u8* coded, u8* decoded, u8 length);


#endif /* HUFFMANCALCULATIONS_H_ */
